import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigModule } from '@nestjs/config';
import { setupSwagger } from './swagger.config';
import { ValidationPipe } from '@nestjs/common';

ConfigModule.forRoot()

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(
    new ValidationPipe()
  )
  setupSwagger(app);
  const port = process.env.PORT
  await app.listen(port);
  console.log("app is running on port", port)
}
bootstrap();
