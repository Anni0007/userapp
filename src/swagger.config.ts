import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';


export function setupSwagger(app) {
    const options = new DocumentBuilder()
      .setTitle('UserAppApis')
      .setDescription('Crud api for user')
      .setVersion('1.0')
      .build();
  
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('swagger', app, document);
  }
  