import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNumber, IsArray } from 'class-validator';

export class CreateUserDto {
  // @ApiProperty({ example: '550e8400-e29b-41d4'})
  // @IsString()
  // id: string;

  @ApiProperty({ example: 'Anirudh'})
  @IsString()
  username: string;

  @ApiProperty({ example: 12})
  @IsNumber()
  age: number;

  @ApiProperty({ example: ['reading', 'swimming']})
  @IsArray()
  @IsString({each: true})
  hobbies: string[];
}