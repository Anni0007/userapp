import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString, IsInt, IsArray } from 'class-validator';

export class UpdateUserDto {

  @ApiProperty({ example: 'Anirudh'})
  @IsOptional()
  @IsString()
  username?: string;

  @ApiProperty({ example: 12})
  @IsOptional()
  @IsInt()
  age?: number;

  @ApiProperty({ example: ['reading', 'swimming']})
  @IsOptional()
  @IsArray()
  hobbies?: string[];
}