import { Body, Controller, Delete, Get, Param, Post, Put, Req, Res } from '@nestjs/common';
import { ApiBody, ApiParam, ApiTags } from '@nestjs/swagger';
import { CreateUserDto } from './dtos/create-user.dto';
import { UsersService } from './users.service';
import { Request, Response } from 'express';
import { UpdateUserDto } from './dtos/update-user.dto';

@Controller('api')
@ApiTags('UserApi')
export class UsersController {
    constructor(private readonly userService: UsersService) {}
    @Get('/users')
    getUsers(@Req() req: Request, @Res() res: Response) {
        return this.userService.getUsers(res);
    }

    @Get('/users/:userId')
    getUserById(@Req() req: Request, @Res() res: Response, @Param('userId') id: string) {
        return this.userService.getUserById(id, res)    }

    @Post('/users')
    @ApiBody({type: CreateUserDto})
    saveUser(@Req() req: Request, @Res() res: Response, @Body() body: CreateUserDto) {
        return this.userService.saveUser(res, body)
    }

    @Put('/users/:userId')
    @ApiParam({ name: 'userId', type: 'string' })
    @ApiBody({ type: UpdateUserDto })
    updateUserById(@Res() res: Response, @Param('userId') id: string, @Body() data: UpdateUserDto) {
        return this.userService.updateUserById(res, data, id)
    }

    @Delete('/users/:userId')
    @ApiParam({ name: 'userId', type: 'string' })
    deleteUserById(@Res() res: Response, @Param('userId') id: string) {
        return  this.userService.deleteUserById(res, id);
    }

}
