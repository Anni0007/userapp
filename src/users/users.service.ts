import { Injectable } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import { UpdateUserDto } from './dtos/update-user.dto';
@Injectable()
export class UsersService {
  private users = [];

  getUsers(res){
    try {
      return res.status(200).send(this.users)
    } catch (error) {
      return res.status(400).send(error.message)
    }
  }

  getUserById(id: any , res) { 
    try {
      const user =  this.users.filter((user)=> user.id === id)

      if (user.length > 0) 
        return res.status(200).send(user)
      else
       return res.status(404).send("User not found")
    } catch (error) {
      return res.status(400).send(error.message)
    }   
  }

  saveUser(res, data) {
      try {
        const userData = data;
        userData.id = uuidv4()       
        this.users.push(userData);
        return res.status(201).send(userData)
      } catch (error) {
        return res.status(400).send(error.message)
      }
  }

  updateUserById(res, data: UpdateUserDto, id: string) {
    try {
      const userIndex = this.users.findIndex(user => user.id === id);
      if (userIndex === -1) {
        return res.status(400).send ('User Not found')
      }
  
      const updatedUser = { ...this.users[userIndex], ...data };
      this.users[userIndex] = updatedUser;
      return res.status(200).send({message:"User updated successfully", updatedUser:updatedUser});
    } catch (error) {
      return res.status(400).send(error.message)
    } 
  }

  deleteUserById(res, id: string) {
    try {
      const userIndex = this.users.findIndex(user => user.id === id);
    if (userIndex === -1) {
      return res.status(400).send('User not found in database');
    }

    const deletedUser = this.users[userIndex];
    this.users.splice(userIndex, 1);
      return res.status(200).send('User deleted successfully')  
    } catch (error) {
      return res.status(400).send(error.message)
    }  
  }

}